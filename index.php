<?php
require_once './apps/Methods.php';
$list = Methods::getComplexList($_SERVER['QUERY_STRING']);
Methods::log('page load');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Shopping List</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
		<script>var list = <?= json_encode($list) ?>, items = <?= json_encode(Methods::getItems(Methods::pluck($list['list_items'], 'id'))) ?></script>	
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.2/jquery.ui.touch-punch.min.js"></script>
		<script src="js/main.js"></script>
    </head>
    <body>
		<div ng-app="shoppingList">
			<div class="row">
				<div ng-controller="CurrentList">					
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="main-list-container">
									<div class="row">
										<div class="col-md-12">
											<input type="text" class="form-control"  ng-model="list.name" />							
										</div>							
									</div>
									<div class="row">
										<div class="col-md-12">
											<ul ng-show="list.list_items.length" sortable="list.list_items" class="main-list">
												<li ng-repeat="list_item in list.list_items">
													<span class="sortable-handle">
														<img src="images/move16.png" />
													</span>
													<label>
														<input type="checkbox" ng-model="list_item.is_checked" />
														{{list_item.item.name}}
													</label>
													<input type="number" size="3" ng-model="list_item.amount" />
													<a href="" class="delete" ng-click="list.list_items.splice($index, 1)">Delete</a>
												</li>
											</ul>																	
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 text-center">
											<button type="button" class="btn btn-default" ng-click="saveList(list)" ng-disabled="!isSynced">Save list</button>
											<div ng-hide="list.list_items.length">No items selected yet.</div>																		
										</div>
									</div>
								</div>								
							</div>
						</div>
						
						
					</div>
				</div>
				<div ng-controller="AvailableItems">
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="row">
									<div class="col-md-12">
										<input type="text" class="form-control" placeholder="Search..." ng-model="searchList" ng-change="console.log(!23)"/>							
									</div>							
								</div>
								<div class="row">
									<div class="col-md-12">
										<div ng-repeat="item in items | filter:searchList">
											<span class="item">{{item.name}}</span><a href="" ng-click="addToList(item)">Add</a>
										</div>
										<div ng-show="!(items | filter:searchList).length && !searching">
											No matching items found
										</div>
									</div>
								</div>
							</div>
								
							</div>
					</div>
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<h2>Create a new item</h2>
								<div class="form-horizontal">
									<div class="form-group">
										<div class="col-md-12">
											<input type="text" class="form-control" placeholder="name" ng-model="newItem.name" />							
										</div>															
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<input type="text" class="form-control" placeholder="Description" ng-model="newItem.description" />							
										</div>															
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 text-center">
										<button type="button" class="btn btn-default" ng-click="addItem(newItem); newItem = null" ng-disabled="!newItem.name">Add a new item</button>										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </body>
</html>
