function link(links, sources, prop, newProp) {
	angular.forEach(links, function(link) {
		var item = sources[link[prop]];
		link[newProp] = item;
		delete(link[prop]);			
	});
}
function booleanize(arrays) {
	angular.forEach(arrays, function(array) {
		angular.forEach(array, function(value, key) {
			if (key.slice(0, 3) === 'is_') {
				array[key] = isNaN(value) ? !!value : !!+value;
			} else if (!isNaN(value)) {
				array[key] = +value;
			}
		});
	});
}
function arrayize(obj) {
	var ret = [];
	angular.forEach(obj, function(value) {
		ret.push(value);
	});
	return ret;
}
link(list.list_items, items, 'item_id', 'item');
booleanize(list.list_items);
var itemsArray = arrayize(items);

var app = angular.module('shoppingList', []);
app.directive('sortable', function() {
	return {
		link: function(scope, elem, attrs) {
			var startingIndex;
			$(elem).sortable({
				placeholder: 'ui-state-highlight',
				handle: '.sortable-handle',
				revert: $.fx.speeds.fast,
				start: function(event, ui) {
					startingIndex = ui.item.index();
				},
				update: function(event, ui) {
					var endingIndex = ui.item.index(),
						obj = scope.$eval(attrs.sortable);
					obj.splice(endingIndex, 0, obj.splice(startingIndex, 1)[0]);
					scope.$apply()
				}
			});
		}
	}
})
app.controller('CurrentList', ['$scope', '$http', '$timeout', function($scope, $http, $timeout) {
	$scope.list = list;
	$scope.saveList = function(list) {
		$http.post('ajax.php', {action: 'save_list', list: list})
			.success(function(id) {
				if (!list.id) {
					location.replace('?' + id)
				} else {
					$scope.isSynced = false;
				}
			})
			.error(function(res) {
				alert('An error occured');
			});
	};
	$timeout(function() {
		//this triggers when setting up the scope, so we need to cancel this the first time
		$scope.isSynced = false;
	})
	$scope.$watch('list.list_items', function() {
		$scope.isSynced = true;
	}, true);
	$scope.$watch('list', function() {
		$scope.isSynced = true;
	}, true);
}]);
app.controller('AvailableItems', ['$scope', '$http', function($scope, $http) {
	$scope.items = itemsArray;
	$scope.addToList = function(item) {
		var found = $.grep(list.list_items, function(obj) { return +obj.item.id === +item.id });
		if (found.length) {
			found[0].amount++;
		} else {
			list.list_items.push({
				id: item.id,
				amount: 1,
				is_checked: false,
				item: item
			})
		}
	};
	$scope.addItem = function(newItem) {
		$http.post('ajax.php', {action: 'add_item', item: newItem})
			.success(function(newId) {
				items[newId] = {
					id: newId,
					name: newItem.name,
					description: newItem.description
				};
				itemsArray.push(items[newId]);
			})
			.error(function(res) {
				alert('An error occured');
			});
	};
}]);
