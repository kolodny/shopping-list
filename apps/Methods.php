<?php

require_once __DIR__ . DIRECTORY_SEPARATOR . 'Db.php';

class Methods {
	public static function getComplexList($url_slug) {
		$list = Db::prepare('SELECT * FROM list WHERE url_slug = :url_slug')->execute(array(':url_slug' => $url_slug))->one();
		$list_items = Methods::sanitizeArray(
			Db::query('SELECT * FROM list_item WHERE list_id = ' . ((int) $list['id']) . ' ORDER BY weight ASC')->all(),
			array('id', 'amount', 'is_checked', 'item_id')
		);
		//$current_items = Db::query('SELECT * FROM item WHERE id IN (' . implode(',', Methods::pluck($list_items, 'item_id')) . ')')->all();
		
		$list['list_items'] = $list_items;
		return $list;
	}
	
	public static function getItems($list_of_ids) {
		//$items = Db::query('SELECT * FROM item WHERE 1 = 1 OR id IN (' . implode(',', $list_of_ids) . ')')->all(); // get all items for now
		$items = Db::query('SELECT * FROM item WHERE 1 = 1')->all(); // get all items for now
		return Methods::reKey($items, 'id');
	}

	public static function pluck($arr, $prop) {
		$return = array();
		foreach ($arr as $a) {
			$return[] = $a[$prop];
		}
		return $return;
	}
	public static function reKey($arr, $new_key, $one_to_many = false, $unset = false) {
		$return = array();
		foreach ($arr as $a) {
			if ($one_to_many) {
				$return[$a[$new_key]][] = $a;
			} else {
				$return[$a[$new_key]] = $a;
			}
			if ($unset) {
				unset($a[$new_key]);
			}
		}
		return $return;
	}
	public static function sanitizeArray($arrays, $list, $fill_instead = null) {
		$return = array();
		foreach ($arrays as $array) {
			$r = array();
			foreach ($array as $key => $value) {
				if (isset($array[$key]) && in_array($key, $list)) {
					$r[$key] = $value;
				} elseif ($fill_instead !== null) {
					$r[$key] = $fill_instead;
				}
			}
			$return[] = $r;
		}
		return $return;
	}
	
	public static function log($name) {
		$dump = array(
			'$_SERVER' => $_SERVER,
			'$_REQUEST' => $_REQUEST,
		);
		Db::prepare("INSERT INTO  `shopping_list`.`log` (`ip`,`action`,`dump`) VALUES (:ip, :action, :dump)")->execute(array(
			'ip' => $_SERVER['REMOTE_ADDR'],
			'action' => $name,
			'dump' => print_r($dump, true),
		));
	}
}