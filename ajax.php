<?php

//header('Content-type: application/json');
$post = !empty($_GET) ? $_GET : json_decode(file_get_contents("php://input"), JSON_OBJECT_AS_ARRAY);
//print_r($post);
//exit();

require_once './apps/Methods.php';

switch ($post['action']) {
	case 'add_item':
		Methods::log('add_item');
		echo Db::prepare("INSERT INTO  `shopping_list`.`item` (`name` ,`description`, `created_by`) VALUES (:name, :description, :created_by)")->execute(array(
			'name' => $post['item']['name'],
			'description' => @$post['item']['description'] ?: '',
			'created_by' => $_SERVER['REMOTE_ADDR'],
		))->lastInsertId();
		break;

	case 'save_list':
		Methods::log('save_list');
		echo save_list($post['list']);
		break;
	
	default:
		break;
}

function save_list($list) {	
	$list_db = Db::prepare('SELECT * FROM list WHERE url_slug = :url_slug')->execute(array(':url_slug' => @$list['url_slug']))->one();
	if (!$list_db) {
		$url_slug = substr(str_shuffle("23456789abcdefghijkmnpqrtuvwxyABCDEFGHJKLMNPQRSTUVWXY"), 0, 4);
		$list_id = Db::prepare('INSERT INTO  `shopping_list`.`list` (`url_slug`) VALUES (:url_slug)')->execute(array(
			'url_slug' => $url_slug,
		))->lastInsertId();
		$list_db['url_slug'] = $url_slug;
		$list_db['id'] = $list_id;
	}
	Db::prepare('UPDATE  `shopping_list`.`list` SET `name` = :name WHERE `list`.`id` = :id')->execute(array(
		'name' => $list['name'],
		'id' => $list_db['id'],
	));
	Db::prepare('DELETE FROM shopping_list.list_item WHERE list_id = :list_id')->execute(array('list_id' => $list_db['id']));
	if ($list['list_items']) {
		foreach ($list['list_items'] as $list_item) {
			$item_insert_array = array(
				'item_id' => $list_item['item']['id'],
				'amount' => $list_item['amount'],
				'is_checked' => $list_item['is_checked'] ? 1 : 0
			);
			$item_insert_array['list_id'] = $list_db['id'];
			Db::prepare(
				'INSERT INTO  `shopping_list`.`list_item` (`list_id`, `item_id`, `amount`, `is_checked`) VALUES (:list_id, :item_id, :amount, :is_checked)'
			)->execute($item_insert_array);
		}
	}
	return $list_db['url_slug'];
}

